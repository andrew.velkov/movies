# Movies

![alt text](http://i.piccy.info/i9/b9c57af0593a04a82ff94ef17345235d/1537365464/296436/1270285/01.jpg)
![alt text](http://i.piccy.info/i9/c34797f5430eb92573124e4dfededbb6/1537365492/105013/1270285/2.jpg)
![alt text](http://i.piccy.info/i9/0d2f9c250c8126360f53d052dc355243/1537365506/58169/1270285/3.png)

```
$ git clone https://gitlab.com/andrew.velkov/movies.git
```

```
$ cd movies
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start:mock
```

Visit `http://localhost:3003/` 