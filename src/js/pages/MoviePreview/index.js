import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getMovies } from 'redux/reducers/movies';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import ListItem from 'components/Movies/ListItem';

import css from 'style/pages/Main';

@connect((state, props) => ({
  movies: state.movies.get,
  movieId: props.match.params.id,
}),
{ getMovies }
)
export default class Movies extends Component {
  static propTypes = {
    movies: PropTypes.object,
    getMovies: PropTypes.func,
    movieId: PropTypes.string,
  };

  componentDidMount() {
    this.props.getMovies();
  }

  render() {
    const { movieId, movies: { data, loaded, loading } } = this.props;
    const movie = loaded && data.find(item => item.id === +movieId);

    return (
      <section className={ css.wrap }>
        <div className={ css.wrap__container }>
          <h2 className={ css.wrap__title }>Movie <span>{loaded && movie.title}</span></h2>

          <Fetching isFetching={ loading } size={ 42 } thickness={ 8 }>
            <section>
              <div className={ cx(css.movies, css.movies_box) }>
                {(loaded && data.length > 0) && <ListItem movie={ movie } isText={ true } />}
              </div>

              <ul className={ cx(css.buttonGroup, css.buttonGroup_end) }>
                <li className={ css.buttonGroup__item }>
                  <Button className={ css.buttonGroup__link } typeButton='raiseLink' to='/' primary={ true }>Back to movies</Button>
                </li>
                <li className={ css.buttonGroup__item }>
                  <Button className={ css.buttonGroup__link } typeButton='raiseLink' to={ `/movie/${ movie.id }/edit` } secondary={ true }>Edit movie</Button>
                </li>
              </ul>
            </section>
          </Fetching>

        </div>
      </section>
    );
  }
}
