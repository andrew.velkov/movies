import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { createMovie } from 'redux/reducers/movies';
import Button from 'components/Button';
import Fields from 'components/Movies/Fields';

import css from 'style/pages/Main';

@connect(() => ({}), { createMovie })
export default class AddMovie extends Component {
  static propTypes = {
    createMovie: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      year: '',
      runtime: '',
      genres: '',
      director: '',
      actors: '',
      plot: '',
      posterUr: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const { genres } = this.state;
    const data = { ...this.state, genres: genres.split(', ') };

    await this.props.createMovie(data);
    this.formReset();
  }

  formReset = () => {
    this.setState({
      title: '',
      year: '',
      runtime: '',
      genres: '',
      director: '',
      actors: '',
      plot: '',
      posterUrl: '',
    });
  }

  render() {
    const { title, plot, posterUrl } = this.state;
    const isValid = title === '' || plot === '' || posterUrl === '';

    return (
      <section className={ cx(css.wrap) }>
        <div className={ css.wrap__container }>
          <h2 className={ css.wrap__title }>Add Movie</h2>
          <form className={ cx(css.movies, css.movies_box) } onSubmit={ this.handleSubmit }>

            <Fields value={ this.state } onChange={ (e) => this.handleChange(e) } />

            <ul className={ cx(css.buttonGroup, css.buttonGroup_between) }>
              <li className={ css.buttonGroup__item }>
                <Button type='submit' primary={ true } disabled={ isValid }>Save</Button>
              </li>
              <ul className={ css.buttonGroup }>
                <li className={ css.buttonGroup__item }>
                  <Button typeButton='flat' onClick={ this.formReset } secondary={ true }>Reset</Button>
                </li>
                <li className={ css.buttonGroup__item }>
                  <Button typeButton='flatLink' to='/' className={ css.todoList__link }>Cancel</Button>
                </li>
              </ul>
            </ul>
          </form>
        </div>
      </section>
    );
  }
}
