import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getMovies, removeMovie } from 'redux/reducers/movies';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import List from 'components/Movies/List';

import css from 'style/pages/Main';

@connect(state => ({
  movies: state.movies.get,
}),
{ getMovies, removeMovie }
)
export default class Movies extends Component {
  static propTypes = {
    movies: PropTypes.object,
    getMovies: PropTypes.func,
    removeMovie: PropTypes.func,
  };

  componentDidMount() {
    this.props.getMovies();
  }

  render() {
    const { data, loading } = this.props.movies;

    return (
      <section className={ css.wrap }>
        <div className={ css.wrap__container }>
          <div className={ css.wrap__header }>
            <h2 className={ css.wrap__title }>Movies List</h2>
            <Button typeButton='raiseLink' to='/movie/add' primary={ true }>Add movie</Button>
          </div>
          <div className={ css.movies }>
            <Fetching isFetching={ loading } size={ 42 } thickness={ 8 }>
              <List movies={ data } remove={ this.props.removeMovie } load={ this.props.getMovies } />
            </Fetching>
          </div>
        </div>
      </section>
    );
  }
}
