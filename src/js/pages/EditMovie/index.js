import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getMovies, editMovie } from 'redux/reducers/movies';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import Fields from 'components/Movies/Fields';

import css from 'style/pages/Main';

@connect((state, props) => ({
  movies: state.movies.get,
  movieId: props.match.params.id,
}),
{ getMovies, editMovie }
)
export default class EditMovie extends Component {
  static propTypes = {
    movies: PropTypes.object,
    movieId: PropTypes.string,
    getMovies: PropTypes.func,
    editMovie: PropTypes.func,
  };

  constructor(props) {
    super(props);
    const { title, year, runtime, genres, director, actors, plot, posterUrl } = this.props.movies.data;
    this.state = { title, year, runtime, genres, director, actors, plot, posterUrl };
  }

  componentDidMount() {
    this.props.getMovies();
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init = (props) => {
    const { movieId, movies: { data, loaded } } = props;

    if (loaded) {
      const movie = data.find((item) => item.id === +movieId);

      this.setState({
        title: movie.title,
        year: movie.year,
        runtime: movie.runtime,
        genres: movie.genres.join(', '),
        director: movie.director,
        actors: movie.actors,
        plot: movie.plot,
        posterUrl: movie.posterUrl,
      });
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit = async (e, isValid) => {
    e.preventDefault();

    const { movieId } = this.props;

    const { genres } = this.state;
    const data = { ...this.state, genres: genres.split(', ') };

    if (!isValid) {
      await this.props.editMovie(+movieId, data);
    }

    this.props.getMovies();
  }

  formReset = () => {
    this.setState({
      title: '',
      year: '',
      runtime: '',
      genres: '',
      director: '',
      actors: '',
      plot: '',
      posterUrl: '',
    });
  }

  render() {
    const { movieId, movies: { loading } } = this.props;
    const { title, plot, posterUrl } = this.state;
    const isValid = title === '' || plot === '' || posterUrl === '';

    return (
      <section className={ cx(css.wrap) }>
        <div className={ css.wrap__container }>
          <h2 className={ css.wrap__title }>Edit Movie <span>{ title }</span></h2>

          <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
            <form className={ cx(css.movies, css.movies_box) } onSubmit={ (e) => this.handleSubmit(e, isValid) }>

              <Fields value={ this.state } onChange={ (e) => this.handleChange(e) } />

              <div className={ cx(css.buttonGroup, css.buttonGroup_between) }>
                <ul className={ css.buttonGroup }>
                  <li className={ css.buttonGroup__item }>
                    <Button type='submit' primary={ true } disabled={ isValid }>Save</Button>
                  </li>
                  <li className={ css.buttonGroup__item }>
                    <Button typeButton='raiseLink' to={ `/movie/${ +movieId }` } className={ css.todoList__link } secondary={ true }>Preview</Button>
                  </li>
                </ul>

                <ul className={ css.buttonGroup }>
                  <li className={ css.buttonGroup__item }>
                    <Button typeButton='flat' onClick={ this.formReset } secondary={ true }>Reset</Button>
                  </li>
                  <li className={ css.buttonGroup__item }>
                    <Button typeButton='flatLink' to='/' className={ css.todoList__link }>Cancel</Button>
                  </li>
                </ul>
              </div>
            </form>
          </Fetching>

        </div>
      </section>
    );
  }
}
