import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover/Popover';
import { Menu, MenuItem } from 'material-ui/Menu';

import Button from 'components/Button';

export default class PopoverConfigurable extends Component {
  static propTypes = {
    iconName: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
  }

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      anchorOrigin: {
        horizontal: 'middle',
        vertical: 'center',
      },
      targetOrigin: {
        horizontal: 'middle',
        vertical: 'center',
      },
    };
  }

  handleClick = (event) => {
    event.preventDefault();
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const { onClick, iconName, className } = this.props;

    return (
      <div>
        <div className={ className }>
          <Button typeButton='icon' iconName={ iconName } onClick={ this.handleClick } />
        </div>

        <Popover
          open={ this.state.open }
          anchorEl={ this.state.anchorEl }
          anchorOrigin={ this.state.anchorOrigin }
          targetOrigin={ this.state.targetOrigin }
          onRequestClose={ this.handleRequestClose }
        >
          <Menu>
            <MenuItem primaryText='Deleted' onClick={ onClick } />
            <MenuItem primaryText='Cancel'onClick={ this.handleRequestClose } />
          </Menu>
        </Popover>
      </div>
    );
  }
}
