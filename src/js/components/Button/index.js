import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { RaisedButton, FlatButton, IconButton, FloatingActionButton } from 'material-ui';
import { NavLink } from 'react-router-dom';

import css from 'style/components/Button';

const Button = ({
  typeButton = 'raise',
  iconName,
  to,
  className,
  onClick,
  children,
  ...button
}) => (
  <span>
    {typeButton === 'raise' &&
      <RaisedButton className={ className } label={ children } onClick={ onClick } { ...button } />
    }
    {typeButton === 'raiseLink' &&
      <NavLink to={ to } className={ className }>
        <RaisedButton label={ children } onClick={ onClick } { ...button } />
      </NavLink>
    }

    {typeButton === 'flat' &&
      <FlatButton className={ className } label={ children } onClick={ onClick } { ...button } />
    }
    {typeButton === 'flatLink' &&
      <NavLink to={ to } className={ className }>
        <FlatButton label={ children } onClick={ onClick } { ...button } />
      </NavLink>
    }

    {typeButton === 'icon' &&
      <IconButton className={ className } onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </IconButton>
    }
    {typeButton === 'iconLink' &&
      <NavLink to={ to } className={ className }>
        <IconButton onClick={ onClick }>
          {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
        </IconButton>
      </NavLink>
    }

    {typeButton === 'circle' &&
      <FloatingActionButton onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </FloatingActionButton>
    }
  </span>
);

Button.propTypes = {
  typeButton: PropTypes.string,
  iconName: PropTypes.string,
  className: PropTypes.string,
  to: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
};

export default Button;
