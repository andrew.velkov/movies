import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const Input = ({ label, name, value, onChange, error, ...input }) => (
  <TextField
    floatingLabelText={ label }
    fullWidth={ true }
    name={ name }
    value={ value }
    errorText={ error }
    onChange={ onChange }
    { ...input }
  />
);

Input.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
};

export default Input;
