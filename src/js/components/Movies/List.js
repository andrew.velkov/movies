import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from 'components/Button';
import Popover from 'components/Popover';
import ListItem from 'components/Movies/ListItem';

import css from 'style/pages/Main';

export default class List extends Component {
  static propTypes = {
    movies: PropTypes.array,
    remove: PropTypes.func,
    load: PropTypes.func,
  };

  removeItem = (e, id) => {
    this.props.remove(id);
    this.props.load();
  }

  render() {
    const { movies } = this.props;

    return (
      <ul className={ css.movies__list }>
        {movies.reverse().map(movie => {
          return (
            <li className={ css.movies__item } key={ movie.id }>
              <ListItem movie={ movie } />

              <div className={ cx(css.buttonGroup, css.buttonGroup_between, css.movies__actions) }>
                <Button typeButton='flatLink' to={ `movie/${ movie.id }` }>More...</Button>
                <div className={ css.buttonGroup__actions }>
                  <Button typeButton='iconLink' iconName='edit' to={ `movie/${ movie.id }/edit` } />
                  <Popover iconName='delete' onClick={ (e) => this.removeItem(e, movie.id) } />
                </div>
              </div>
            </li>
          );
        })}
      </ul>
    );
  }
}
