import React from 'react';
import PropTypes from 'prop-types';

import Input from 'components/Form/Input';

const Fields = ({ value, onChange }) => (
  <div>
    <Input label='Title *' name='title' value={ value.title } onChange={ onChange } />
    <Input label='Year' name='year' value={ value.year } onChange={ onChange } />
    <Input label='Runtime' name='runtime' value={ value.runtime } onChange={ onChange } />
    <Input label='Genres (use ", " as a separator)' name='genres' value={ value.genres } onChange={ onChange } />
    <Input label='Director' name='director' value={ value.director } onChange={ onChange } />
    <Input label='Actors' name='actors' value={ value.actors } onChange={ onChange } />
    <Input label='Image *' name='posterUrl' value={ value.posterUrl } onChange={ onChange } />
    <Input label='Plot*' name='plot' value={ value.plot } multiLine={ true } onChange={ onChange } />
  </div>
);

Fields.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
};

export default Fields;
