import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import css from 'style/pages/Main';

const getTimeFromMins = (mins) => {
  const hours = Math.trunc(mins / 60);
  const minutes = mins % 60;

  return `${ hours }h ${ minutes }m`;
};

const ListItem = ({ isText = false, movie }) => {
  const genres = movie.genres.map(genre => <Link key={ genre } to=''> { genre }, </Link>);
  /* eslint-disable */
  const plot = movie.plot && movie.plot.split('\n').map((item) => <span>{ item }<br /></span>);

  return (
    <div className={ css.movies__wrap }>
      <img className={ css.movies__image } src={ movie.posterUrl } alt='' />

      <div className={ css.movies__body }>
        <h4 className={ css.movies__title }>{ movie.title }</h4>
        <p><b>Year:</b> { movie.year }</p>
        <p><b>Runtime:</b> { getTimeFromMins(movie.runtime) }</p>
        <p><b>Genre:</b> { genres }</p>
        <p><b>Director:</b> { movie.director }</p>

        {isText && <div className={ css.movies__text }>
          <hr />
          <p><b>Actors:</b> { movie.actors }</p>
          <p><b>Plot:</b> { plot }</p>
        </div>}

      </div>
    </div>
  );
};

ListItem.propTypes = {
  movie: PropTypes.object,
  isText: PropTypes.bool,
};

export default ListItem;
