import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { CircularProgress } from 'material-ui';

import css from 'style/components/Fetching';

const Fetching = ({ color = '#c4cdcf', size = 26, thickness = 5, isFetching, children, position }) => (
  <article className={ css.fetching }>
    {isFetching && <div className={ cx(css.fetching__wrap, css[`fetching__wrap_${ position }`]) }>
      <CircularProgress color={ color } size={ size } thickness={ thickness } />
    </div>}
    { children }
  </article>
);

Fetching.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number,
  isFetching: PropTypes.bool,
  position: PropTypes.string,
  children: PropTypes.any,
};

export default Fetching;
