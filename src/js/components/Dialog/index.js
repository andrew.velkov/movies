import React from 'react';
import PropTypes from 'prop-types';
import { Dialog } from 'material-ui';
import { Link } from 'react-router-dom';

import Button from 'components/Button';

import css from 'style/components/Dialog';

export default class DialogExample extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    buttonSend: PropTypes.bool,
    disabled: PropTypes.bool,
    open: PropTypes.bool,
    onClick: PropTypes.func,
    title: PropTypes.string,
    buttonName: PropTypes.string,
    className: PropTypes.string,
  }

  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { buttonType = 'button', buttonSend = true, buttonName, title, disabled, onClick, children, ...params } = this.props;
    const actions = [
      <span>
        {buttonSend && <Button
          typeButton='flat'
          label='Send'
          disabledClass={ disabled }
          onClick={ onClick }
        />}
      </span>,
      <Button
        typeButton='flat'
        label='Cancel'
        onClick={ this.handleClose }
      />,
    ];

    return (
      <span>
        {buttonType === 'link' &&
          <Link to='#' onClick={ this.handleOpen } { ...params }>{ buttonName }</Link>
        }

        {buttonType === 'button' &&
          <Button onClick={ this.handleOpen } { ...params }>{ buttonName }</Button>
        }

        {buttonType === 'icon' &&
          <Button typeButton='icon' iconName={ buttonName } onClick={ this.handleOpen } { ...params } />
        }

        {this.state.open && <Dialog
          title={ title }
          actions={ actions }
          modal={ false }
          open={ this.state.open }
          onRequestClose={ this.handleClose }
          contentClassName={ css.dialog__content }
          titleClassName={ css.dialog__title }
          bodyClassName={ css.dialog__body }
          actionsContainerClassName={ css.dialog__actions }
        >
          { children }
        </Dialog>}
      </span>
    );
  }
}
