import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';
import App from 'containers/App';

import Movies from 'pages/Movies';
import MoviePreview from 'pages/MoviePreview';
import EditMovie from 'pages/EditMovie';
import AddMovie from 'pages/AddMovie';
import NotFound from 'pages/NotFound';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <App>
          <Switch>
            <Route exact path='/' render={ (props) => <Movies { ...props } /> } />
            <Route path='/movie/add' render={ (props) => <AddMovie { ...props } /> } />

            <Route exact path='/movie/:id' render={ (props) => <MoviePreview { ...props } /> } />
            <Route exact path='/movie/:id/edit' render={ (props) => <EditMovie { ...props } /> } />
            <Route component={ NotFound } />
          </Switch>
        </App>
      </Router>
    );
  }
}
