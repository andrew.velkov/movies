const apiMiddleWare = () => next => action => {
  const { types, request: { method = 'GET', url, body }, ...rest } = action;

  if (!types) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({ ...rest, type: REQUEST });

  return fetch(encodeURI(url), {
    method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then(res => {
      if (res.status >= 200 && res.status < 300) {
        return res.json();
      }

      if (res.status === 401) {
        return next({ ...rest, type: FAILURE });
      }

      return res.json();
    })
    .then(data => next({
      ...rest,
      type: SUCCESS,
      payload: data,
    }))
    .catch(error => next({
      ...rest,
      type: FAILURE,
      payload: error.message,
    }));
};

export default apiMiddleWare;
