import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';

import reducer from './reducers';
import apiMiddleWare from './middleware/apiMiddleware';

const store = createStore(
  reducer,
  compose(
    applyMiddleware(apiMiddleWare, logger),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

export default store;
