const GET_MOVIES = 'movies/GET_MOVIES';
const GET_MOVIES_SUCCESS = 'movies/GET_MOVIES_SUCCESS';
const GET_MOVIES_ERROR = 'movies/GET_MOVIES_ERROR';

const CREATE_MOVIE = 'movies/CREATE_MOVIE';
const CREATE_MOVIE_SUCCESS = 'movies/CREATE_MOVIE_SUCCESS';
const CREATE_MOVIE_ERROR = 'movies/CREATE_MOVIE_ERROR';

const EDIT_MOVIE = 'movies/EDIT_MOVIE';
const EDIT_MOVIE_SUCCESS = 'movies/EDIT_MOVIE_SUCCESS';
const EDIT_MOVIE_ERROR = 'movies/EDIT_MOVIE_ERROR';

const REMOVE_MOVIE = 'movies/REMOVE_MOVIE';
const REMOVE_MOVIE_SUCCESS = 'movies/REMOVE_MOVIE_SUCCESS';
const REMOVE_MOVIE_ERROR = 'movies/REMOVE_MOVIE_ERROR';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    data: [],
  },
  create: {
    loaded: false,
    loading: false,
    status: '',
  },
  edit: {
    loaded: false,
    loading: false,
    status: '',
  },
  remove: {
    loaded: false,
    loading: false,
    status: '',
  },
};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case GET_MOVIES:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case GET_MOVIES_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.payload.result,
        },
      };
    case GET_MOVIES_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    case CREATE_MOVIE:
      return {
        ...state,
        create: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case CREATE_MOVIE_SUCCESS:
      return {
        ...state,
        create: {
          loading: false,
          loaded: true,
          data: action.payload.result,
          status: 'created',
        },
      };
    case CREATE_MOVIE_ERROR:
      return {
        ...state,
        create: {
          loading: false,
          status: '',
        },
      };
    case EDIT_MOVIE:
      return {
        ...state,
        edit: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case EDIT_MOVIE_SUCCESS:
      return {
        ...state,
        edit: {
          loading: false,
          loaded: true,
          status: 'edited success',
        },
      };
    case EDIT_MOVIE_ERROR:
      return {
        ...state,
        edit: {
          loading: false,
          status: 'edited error',
        },
      };
    case REMOVE_MOVIE:
      return {
        ...state,
        remove: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case REMOVE_MOVIE_SUCCESS:
      return {
        ...state,
        remove: {
          loading: false,
          loaded: true,
          status: 'remove success',
        },
      };
    case REMOVE_MOVIE_ERROR:
      return {
        ...state,
        remove: {
          loading: false,
          status: 'remove error',
        },
      };
    default:
      return state;
  }
}

export const getMovies = () => {
  return {
    types: [GET_MOVIES, GET_MOVIES_SUCCESS, GET_MOVIES_ERROR],
    request: {
      method: 'GET',
      url: '/api/movies',
    },
  };
};

export const createMovie = data => {
  return {
    types: [CREATE_MOVIE, CREATE_MOVIE_SUCCESS, CREATE_MOVIE_ERROR],
    request: {
      method: 'POST',
      url: '/api/movies',
      body: data,
    },
  };
};

export const editMovie = (id, data) => {
  return {
    types: [EDIT_MOVIE, EDIT_MOVIE_SUCCESS, EDIT_MOVIE_ERROR],
    request: {
      method: 'PUT',
      url: `/api/movies/${ id }`,
      body: data,
    },
  };
};

export const removeMovie = id => {
  return {
    types: [REMOVE_MOVIE, REMOVE_MOVIE_SUCCESS, REMOVE_MOVIE_ERROR],
    request: {
      method: 'DELETE',
      url: `/api/movies/${ id }`,
    },
  };
};
