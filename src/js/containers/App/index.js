import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import ErrorBoundary from 'containers/ErrorBoundary';

import css from 'style/containers/App';

const muiTheme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif',
  fontSize: 13,
  palette: {
    primary1Color: '#347ac0',
    accent1Color: '#bf3048',
  },
  button: {
    height: 44,
    minWidth: 120,
    fontSize: 13,
    fontWeight: 600,
  },
  raisedButton: {
    fontWeight: 600,
  },
  flatButton: {
    color: '#f3f3f3',
    fontSize: 13,
    fontWeight: 600,
  },
  checkbox: {
    fontSize: 13,
    boxColor: '#9e9b9b',
    fontWeight: 600,
  },
  radioButton: {
    fontSize: 13,
    borderColor: '#9e9b9b',
    fontWeight: 600,
  },
  toggle: {
    fontSize: 13,
  },
  datePicker: {
    selectColor: '#347ac0',
    headerColor: '#347ac0',
    minWidth: '100%',
    Width: '100%',
  },
  menuItem: {
    hoverColor: '#eee',
    selectedTextColor: '#347ac0',
  },
  chip: {
    fontSize: 13,
  },
});

export default class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    const { children } = this.props;

    return (
      <ErrorBoundary>
        <section className={ css.root__item }>
          <MuiThemeProvider muiTheme={ muiTheme }>
            <div>
              { children }
            </div>
          </MuiThemeProvider>
        </section>
      </ErrorBoundary>
    );
  }
}
